Remove pacakge and all of its dependencies(NOTE: only dependecies which also not used by others packages)
```
sudo apt-get autoremove --purge PACKAGE_NAME
```

List all of the installed packages
```
apt list --installed
```

Check whether interested package had already been installed
```
apt list --installed | grep 'mariadb'
```

### How to remove PPA(Personal Package Archive)
*Method 1*
- Need to know the exact PPA_NAME/ppa
```
sudo add-apt-repository --remove ppa:PPA_NAME/ppa
```

*Method 2*
Step 1: List all PPAs added in Linux OS
```
sudo ls /etc/apt/sources.list.d
```

Step 2:
Step 2: Remove the desired PPA_NAME.list
```
sudo rm -i /etc/apt/sources.list.d/PPA_NAME.list
```

*Method 3*
Step 1: Make sure has ppa-purge package installed on your system, else install with following command:
```
sudo apt-get install ppa-purge
```

Step 2: Remove the ppa-url, the benefit of *Method 3* is removes the PPA but also uninstalls all the programs installed by the PPA
```
sudo ppa-purge ppa-url
```