`sudo apt-get update`

`sudo apt-cache search php7`

`sudo apt-get install libapache2-mod-php7.0 php-all-dev php7.0 php7.0-cgi php7.0-cli php7.0-common php7.0-curl php7.0-dev php7.0-gd php7.0-gmp php7.0-json php7.0-ldap php7.0-mysql php7.0-odbc php7.0-opcache php7.0-pgsql php7.0-pspell php7.0-readline php7.0-recode php7.0-snmp php7.0-sqlite3 php7.0-tidy php7.0-xml php7.0-xmlrpc libphp7.0-embed php7.0-bcmath php7.0-bz2 php7.0-enchant php7.0-fpm php7.0-imap php7.0-interbase php7.0-intl php7.0-mbstring php7.0-mcrypt php7.0-phpdbg php7.0-soap php7.0-sybase php7.0-xsl php7.0-zip php7.0-dba`


`apachectl configtest` [Check Apache conf syntax](https://httpd.apache.org/docs/2.4/programs/apachectl.html)

`ls -la  /etc/apache2/mods-enabled` List all enable Apache module
`ls -la  /etc/apache2/mods-available` List all available Apache module


## 2018-04-22
USEFUL!
`dpkg -l | less` list out packages been install on ubuntu machine
`dpkg -l | less | grep 'php'` list out packages has `php` keyword that been install on ubuntu machine

ADD REPOSITORY FOR PHP7.2 PACKAGE
sudo add-apt-repository ppa:ondrej/php
sudo add-apt-repository ppa:ondrej/apache2
ppa:ondrej/nginx-mainline

`sudo apt-get purge libapache2-mod-php7.0 php-all-dev php7.0 php7.0-cgi php7.0-cli php7.0-common php7.0-curl php7.0-dev php7.0-gd php7.0-gmp php7.0-json php7.0-ldap php7.0-mysql php7.0-odbc php7.0-opcache php7.0-pgsql php7.0-pspell php7.0-readline php7.0-recode php7.0-snmp php7.0-sqlite3 php7.0-tidy php7.0-xml php7.0-xmlrpc libphp7.0-embed php7.0-bcmath php7.0-bz2 php7.0-enchant php7.0-fpm php7.0-imap php7.0-interbase php7.0-intl php7.0-mbstring php7.0-mcrypt php7.0-phpdbg php7.0-soap php7.0-sybase php7.0-xsl php7.0-zip php7.0-dba`

https://askubuntu.com/questions/866985/phpenmod-mcrypt-and-phpenmod-mbstring-return-errors/866988
https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-phpmyadmin-on-ubuntu-16-04
https://askubuntu.com/questions/148932/how-can-i-get-a-list-of-all-repositories-and-ppas-from-the-command-line-into-an
