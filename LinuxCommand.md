Display your list group ownership
$ users
$ whoami
$ who am i
$ echo $USER

Print primary group membership for a user name dev
id -g -n dev
Print seconday group membership for a user name dev
id -G -n dev

Search /etc/group using grep command
grep devlim /etc/group

Display help page for specific ubuntu command, for example the id command
man id

Find out which group a user is in
groups devlim

List lists of users in a specific group, for example the adm group
grep -w adm /etc/group

Find out the ownership of a file/dir
ls -l /path/to/the/file
or
stat /path/to/the/file
or
stat -c "%U %G" /path/to/the/file

---
http://askubuntu.com/a/244003
chmod -R a+rX directoryname

- chmod is the name of the command, use for changing the permissions of files.
- -R is the recursive flag. It means apply this command to the directory, and all of its children, and of its children's children, and so on.
- a stands for all: apply these permissions the owner of the file, the group owner of the file, and all other users.

- + means add the following permissions if they aren't set already.
- r means the read permission.
- X means the execute permission, but only on directories. Lower-case x would mean the execute permission on both files and directories.

---

add a new user to a group, for example add devlim to www-data group
sudo useradd -g www-data devlim

sudo passwd devlim     //set password for devlim user

add existing user to a group
sudo usermod -a -G www-data devlim

---
[Reference source](https://www.pluralsight.com/blog/tutorials/linux-add-user-command)
###passwd & group file
1. both passwd & group file located at etc/passwd & etc/group respectively
2. both file hold all of the users and group information

####passwd file
1. whenever a user is add, the user is added to the etc/passwd file
2. `cat /etc/passwd` or `less /etc/passwd` to view the passwd file content on terminal
3. `www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin`
  1. the www-data:`x`:33 indicates an encrypted password is store in the `etc/shadow file`
  2. the x:`33`:33 is the User identification number
  3. the x:33:`33` is the primary group ID, which can be found in `/etc/group` file
  4. the www-data:`/var/www`:/usr/sbin/nologin is the Home Directory of the user
  5. the /var/www:`/usr/sbin/nologin` is the shell used for executing commands

####group file
1. hold all the group information as well as users belonging to each group
2. `devlim:x:1000:`
  1. `devlim` refer to the name of the group
  2. `x` indicates encrypted password. Not really used with groups
  3. `1000` is the group id(GUI)
  4. `:` at the end refer to Users that are in the group are listed here

---
`useradd aUserName` add a user
`useradd -d /home/nonDefaultHomeDir aUserName` set home directory for a user, using -d on it own will set the home directory for user but not create, if need create and set user `useradd -d /home/nonDefaultHomeDir -m aUserName`, the default home directory for a user when not specify is `/home/aNewUserName`.
`su aUsername` use aUsername to login, exit will be called to logout the current user, use `-` like `su- aUserName` will execute the login profile, which make all enviroments variables and alias with the user will be available

`userdel aUserName` deleting a user
`userdel existingUserName groupName` remove a user from a group

`passwd ausername` changing ausername password, require login as `Root`
`passwd` changing current login user password

---
`groupadd aGroupName` add a new group
`grep aGroupName /etc/group` check the newsest group information
`groupdel existingGroupName` del a group

---

###### Summary
Commands: useradd, userdel, usermod, groupadd, groupdel
Options
-d change user's home directory

-m create directory

-s used to change the default shell

-r remove home directory when deleting user

"Passwd" will change the user's password

---

###### chmod
*for file*
 * read - can open, read, copy a file
 * write - can modified a file
 * execute - can execute/run a file if the file is executable

*for directory*
 * read - can `ls` directory and see the content
 * write - can create/remove files in that directory
 * execute - can `cd` into the directory

*example of* `ls -l`
 * drwx------ : a directory, owner can read, write, execute.
 * drwxr-xr-x : a directory, anyone can read and execute.
 * -rwxr-xr-x : a file: anyone can read and execute.
 * -rw-r----- : a file: owner and group can read, owner execute only.

*value for* `chmod`
 * 4 - read
 * 2 - write
 * 1 - execute

*Example of* `chmod` <br/>
`chmod 777 dir_name/file_name`

***

`fallocate -1 1GB filename` - create a file with 1GB size, if preallocation is support. preallocation is done quickly by allocating blocks and marking them as uninitialized, requiring no IO to  the  data blocks. This  is  much faster than creating a file by filling it with zeros.


***

Multi-tasking in terminal

Running job in background when starting a script/process <br/>
*eg.* `bash demo.sh &` - The & mean run job in background

`Ctrl Z` - __Stopped__ a process instead of quit, it will return something like <br/>
`[1]+ Stopped`, the [1] is the job id

`bg %1` - return job id `1` in background, `bg %1 %3` is supported.<br/>
`fg %1` - return job id `1` in foreground

`jobs` - listing current process & status




***
http://askubuntu.com/questions/365087/grant-a-user-permissions-on-www-data-owned-var-www
madeby.media


https://www.cs.swarthmore.edu/help/chmod.html
