```ssh-keygen -t rsa```

Create the key pair on the client machine
***

`/User/.ssh/id_rsa.pub` is public key

`/User/.ssh/id_rsa` is a private key
***

`ssh root@[server.ip.address.here]`

Login to server via ssh as root
***
###### The following set-up will automatically kicked out those who trying to SSH directly as root to server from a system that doesn't have its keys shared without being prompted for a root password.

edit the server's SSHd configuration ``/etc/ssh/sshd_config` & update _PermitRootLogin_ to `PermitRootLogin without-password`

Restart the rehup the `sshd` process to have it re-read the new configuration by

`ps auxw | grep ssh`

`kill -HUP 681`

[How To Use SSH Keys with DigitalOcean Droplets via @DigitalOcean Community](https://www.digitalocean.com/community/tutorials/how-to-use-ssh-keys-with-digitalocean-droplets)
