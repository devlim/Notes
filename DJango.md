`mkdir django-project-root-dir`
`cd django-project-root-dir`
`virtualenv env`
`source env/bin/activate`
`pip install django`

`django-admin startproject a-project-name`
`cd a-project-name`
`python3 manage.py migrate`
`python3 manage.py runserver`

`python manage.py startapp a-app-name`
