# Docker Terminology
1. it's not a virtual machine
    - isolation, process isolation
2. Image
    - starting point
    - term for developer/programmer will be class in OOP
3. container
    - running of image
    - term for developer/programmer, object created from a class
4. Host
    - Machine docker is installed on



### Example of a docker command
```
$> docker run -it -d --rm --name demo_name -v $HOME/Desktop/TEMP -w /src_dummy -p 8080:3000 ubuntu /bin/bash
```
docker           - cli
run              - command
-it              - options, interactive docker container style??
-d               - detach mode(without this, terminal will attach to the container once start/running)
--rm             - options, remove container once container process is exit
--name           - set a prefer name to the container, instead of docker random generate container name
-v               - volume, map between host and cotainer directory
-w               - container working directory
-p               - port binding | map host port to container port, can multi port binding
ubuntu /bin/bash - arguments(this case mean ubuntu image and ??)


### MISC
```
docker ps
```
list all running docker container

```
docker ps -a
```
list all the docker container(including docker container which been exit)

```
docker rm <prefix_unique_docker_id/docker_NAMES>
```
remove container(list via `docker ps -a` command)

```
docker start <prefix_unique_docker_id/docker_NAMES>
```
start container which been created and turn off before

```
docker attach <prefix_unique_docker_id/docker_NAMES>
```
attach current terminal to started container

```
docker stop <prefix_unique_docker_id/docker_NAMES>
```
stop a running docker container

```
docker run --rm -v directory_of_the_host folder_on_the_container args
```
created map point between the host, the container has been run from, and the container itself

```
docker logs <prefix_unique_docker_id/docker_NAMES> -f
```
output docker output(latest?/current?) to the terminal
-f foward logs?

```
docker inspect <prefix_unique_docker_id/docker_NAMES>
```
inspect container

```
docker images
```
list all local images(images been pull to local and nvr been remove before)

```
docker rmi images-name
```
remove local docker images

```
docker network ls
```

```
docker network prune
```
- remove all networks not used by at least one container
* * *

Dockerfile
- instruction file
- used to build/create a image in docker
- capable to execute a bunch of command, then create image
- filename is: Dockerfile
```
FROM node:7.7.4-alpine

EXPOSE 3000
RUN mkdir /src
COPY app.js /src
WORKDIR /src
CMD node app.js
```
FROM - always start from FROM
RUN - run a command
COPY - copy app.js from current working directory to container /src directory
WORDIR - container working directory
CMD - command container will execute when container start-up

```
docker build -t nodejs-app .
docker run --rm -p 8080:3000 -d nodejs-app
```
-t - tag the image with a name
. - where to find the docker file, this case is current directory

* * *
docker composer
- represent how your whole articheture look like
- another way to startup a docker container
- describe enviroment that we wanna run using docker
- preferable/common filename: docker-compose.yml
  - using yaml syntax,
- consist of 2 main component
  1. service - container that start you gonna start inside dockerfile
  2. networks - doing isolation
- allow us to pre defined bunch staff like:
  - port binding between host and container
  - specify build from which Dockerfile
  - can has multiple application by specify multi service
- command line is `docker-compose`
  - separate product inside docker family
  - not part of the docker-cli

```
version: "3"

services:
    your_service_name:
        image: dockerimagetopullfrom
        ports:
            - "8080:3000"
        networks:
            - webnet
        build:
            context: ./
            dockerfile: Dockerfile
        container_name: cotaninername
networks:
    webnet:
```

```
docker-compose -f ./docker-compose.yml up node
docker-compose rm -f
```
first line, -f is specify filename
second line, -f is force, force to kill everything in file


##UNSTRUCTURE INFO
access already running container(test via Vanitee API, which start via docker-compose)
```
docker exec -it <container_name/container_id> sh
```
